require 'json'

class ProgrammingLanguages
  attr_reader :programming_languages_list

  def initialize(path_to_json_file:)
    data_json                   = File.read(path_to_json_file)
    @programming_languages_list = JSON.parse(data_json)
  end

  def find_programming_languages(search_phrase:, negative_phrases: nil)
    return all_found_languages(search_phrase) if negative_phrases.nil?

    languages_without_negative_words(search_phrase, negative_phrases)
  end

  private

  def all_found_languages(search_phrase)
    search_phrase = search_phrase.split

    programming_languages_list.select do |language|
      language if add_language_to_found_list?(language, search_phrase)
    end
  end

  def add_language_to_found_list?(language, search_phrase)
    lanquage_data_contains_word?(language, search_phrase).all?(true)
  end

  def languages_without_negative_words(search_phrase, negative_phrases)
    negative_phrases = negative_phrases.to_a

    all_found_languages(search_phrase).delete_if do |language|
      delete_programming_language?(language, negative_phrases)
    end
  end

  def delete_programming_language?(language, negative_phrases)
    lanquage_data_contains_word?(language, negative_phrases).any?(true)
  end

  def lanquage_data_contains_word?(language, phrases)
    phrases.map do |word|
      language.values.any?{ |value| value.include?(word) }
    end
  end
end
