### Programming Languages

#Method 'find_programming_languages' accepts one or two parameters.

- If you need to find all programming languages, which contain your
  search word - you pass only 1 parameter with key 'search_phrase:';
- If you need to find programming languages, which contain your
  search word but do not contain specific words - you pass 2 parameters
  with keys 'search_phrase:' and 'negative_phrases:' accordingly.

#Method 'all_found_languages'

- Returns all found languages ​​relative to the search phrase.

#Method 'add_language_to_found_list?'

- Returns true if lanquage contain search word.

#Method 'languages_without_negative_words'

- Returns languages from all_found_languages method, which
  do not contain specific words(negative_phrases).

#Method 'delete_programming_language?'

- Сhecks whether an item should be removed from 'all_found_languages'
method and returns true or false

#Method 'lanquage_data_contains_word?'

- Сhecks that a particular language contains a specific word.
  Used in both positive and negative cases